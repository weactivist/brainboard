app.factory( "firebaseFactory", [ function() {
	var config = {
		apiKey: "AIzaSyAgJxfH0i1nEnxQLTDTY_sSP29Cywki86o",
		authDomain: "brainboard-5cac0.firebaseapp.com",
		databaseURL: "https://brainboard-5cac0.firebaseio.com",
		storageBucket: ""
	};

	firebase.initializeApp( config );

	return function() {
		return {
			auth: firebase.auth(),
			database: firebase.database(),
			ref: function( path ) {
				return firebase.database().ref( path );
			},
			storage: firebase.storage()
		};
	};
} ] );