app.filter( "toArray", function() {
	return function( obj ) {
		if( !( obj instanceof Object ) ) {
			return obj;
		}

		return Object.keys( obj ).map( function( key ) {
			if( !( obj[key] instanceof Object ) ) {
				return obj[key];
			}

			return Object.defineProperty( obj[key], '$key', { __proto__: null, value: key } );
		} );
	}
} );