/**
 * @constructor
 */
var Likes = function() {
	
};

/**
 * @param firebaseRefId
 * @returns {boolean}
 */
Likes.prototype.Exists = function( firebaseRefId ) {
	return this.Get().hasOwnProperty( firebaseRefId );
};

/**
 * @returns {{}}
 */
Likes.prototype.Get = function() {
	if( !this.HasLocalStorage() )
		return {};

	var likes = localStorage.getItem( "hororscope:likes" );

	return ( likes !== null ) ? JSON.parse( likes ) : {};
};

/**
 * @param firebaseRefId
 * @constructor
 */
Likes.prototype.Set = function( firebaseRefId ) {
	if( !this.HasLocalStorage() )
		return;
	
	var likes = this.Get();

	if( !likes.hasOwnProperty( firebaseRefId ) ) {
		likes[ firebaseRefId ] = true;
	}

	localStorage.setItem( "hororscope:likes", JSON.stringify( likes ) );
};

Likes.prototype.Remove = function( firebaseRefId ) {
	if( !this.HasLocalStorage() )
		return;

	var likes = this.Get();

	if( !likes.hasOwnProperty( firebaseRefId ) ) {
		return;
	}

	delete likes[firebaseRefId];

	localStorage.setItem( "hororscope:likes", JSON.stringify( likes ) );
};

/**
 * @returns {boolean}
 */
Likes.prototype.HasLocalStorage = function() {
	return typeof( Storage ) !== "undefined";
};