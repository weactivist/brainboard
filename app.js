var app = angular.module( "brainboard", [ "ngRoute", "ui.bootstrap" ] );

app.config( function( $routeProvider ) {
	$routeProvider.when( "/", {
		templateUrl: "/views/index.html",
		controller: "indexController"
	} )
	.when( "/board/:id", {
		templateUrl: "/views/board.html",
		controller: "boardController"
	} )
	.when( "/create", {
		templateUrl: "/views/create.html",
		controller: "createController"
	} )
	.otherwise( {
		redirectTo: "/"
	} );
} );