Object.resolve = function( path, obj, safe ) {
	return path.split( "." ).reduce( function( prev, curr ) {
		return !safe ? prev[curr] : ( prev ? prev[curr] : undefined )
	}, obj || self)
};

Object.applyStringVal = function( obj, path, val ) {
	if( typeof path === "string" ) {
		return Object.applyStringVal( obj, path.split( "." ), val );
	}
	else if( path.length === 1 && val !== undefined ) {
		return obj[path[0]] = val;
	}
	else if( path.length === 0 ) {
		return obj;
	}
	else {
		return Object.applyStringVal( obj[path[0]], path.slice( 1 ), val );
	}
};

function getSynchronizedArray( firebaseRef ) {
	var list = [];

	syncChanges( list, firebaseRef );
	wrapLocalCrudOps( list, firebaseRef );

	return list;
}

function syncChanges( list, ref ) {
	ref.on( "child_added", function _add( snap, prevChild ) {
		var data = snap.val();
		var pos = positionAfter( list, prevChild );
		
		data.$id = snap.key;
		list.splice( pos, 0, data );
	} );

	ref.on( "child_removed", function _remove( snap ) {
		var i = positionFor( list, snap.key );

		if( i > -1 ) {
			list.splice( i, 1 );
		}
	});

	ref.on( "child_changed", function _change( snap ) {
		var i = positionFor( list, snap.key );

		if( i > -1 ) {
			list[i] = snap.val();
			list[i].$id = snap.key;
		}
	});

	ref.on( "child_moved", function _move( snap, prevChild ) {
		var curPos = positionFor( list, snap.key );

		if( curPos > -1 ) {
			var data = list.splice( curPos, 1 )[0];
			var newPos = positionAfter( list, prevChild );
			list.splice( newPos, 0, data );
		}
	});
}

function positionFor( list, key ) {
	for( var i = 0, len = list.length; i < len; i++ ) {
		if( list[i].$id === key ) {
			return i;
		}
	}

	return -1;
}

function positionAfter( list, prevChild ) {
	if( prevChild === null ) {
		return 0;
	}
	else {
		var i = positionFor( list, prevChild );
		if( i === -1 ) {
			return list.length;
		}
		else {
			return i+1;
		}
	}
}

function wrapLocalCrudOps( list, firebaseRef ) {
	list.$add = function( data ) {
		return firebaseRef.push( data );
	};

	list.$remove = function( key ) {
		firebaseRef.child( key ).remove();
	};

	list.$set = function( key, newData ) {
		if( newData.hasOwnProperty( "$id" ) ) {
			delete newData.$id;
		}

		if( newData.hasOwnProperty( "$$hashKey" ) ) {
			delete newData.$$hashKey;
		}
		
		firebaseRef.child( key ).set( newData );
	};

	list.$indexOf = function( key ) {
		return positionFor( list, key );
	}
}