var gulp 			= require( "gulp" );
var concat 			= require( "gulp-concat" );
var uglify 			= require( "gulp-uglify" );
var minifyCSS 		= require( "gulp-minify-css" );
var pako 			= require( "gulp-pako" );
var less 			= require( "gulp-less" );

/**
 * Minify JS files
 */
gulp.task( "js", function() {
	return gulp.src( [
			"bower_components/angular/angular.min.js",
			"bower_components/angular-route/angular-route.min.js",
			"bower_components/firebase/firebase.js",
			"bower_components/angular-bootstrap/ui-bootstrap.min.js",
			"bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js",
			"app.js",
			"scripts.js",
			"factories/*.js",
			"services/*.js",
			"directives/*.js",
			"filters/*.js",
			"controllers/*.js",
			"js/*.js"
		] )
		.pipe( uglify( { mangle: false } ) )
		.pipe( concat( "script.min.js" ) )
		.pipe( gulp.dest( "public/dist" ) );
} );

/**
 * Minify CSS files
 */
gulp.task( "css", function() {
	return gulp.src( [
			"bower_components/bootstrap/less/bootstrap.less",
			"style/*.less"
		] )
		.pipe( less() )
		.pipe( minifyCSS() )
		.pipe( concat( "style.min.css" ) )
		.pipe( gulp.dest( "public/dist" ) );
} );

/**
 * Move Fonts
 */
gulp.task( "fonts", function() {
	return gulp.src( "bower_components/bootstrap/fonts/**.*" )
		.pipe( gulp.dest( "public/fonts" ) );
} );

/**
 * Watch JS
 */
gulp.task( "watch-js", function() {
	return gulp.watch( [
		"app.js",
		"scripts.js",
		"factories/*.js",
		"services/*.js",
		"directives/*.js",
		"filters/*.js",
		"controllers/*.js",
		"js/*.js"
	], [ "js" ] );
} );

/**
 * Watch JS
 */
gulp.task( "watch-css", function() {
	return gulp.watch( "style/*.less", [ "css" ] );
} );

gulp.task( "watch", [ "watch-js", "watch-css" ] );

/**
 * Build files
 */
gulp.task( "build", [ "js", "css" ] );

/**
 * Default task
 */
gulp.task( "default", [ "build", "fonts" ] );