app.directive( "hsBindTimestamp", function() {
	return {
		restrict: "A",
		require: "ngModel",
		link: function( scope, element, attrs, ngModel ) {
			ngModel.$formatters.push( function( value ) {
				var date = new Date();

				if( typeof value === "number" ) {
					date = new Date( value );
				}

				date.setSeconds( 0 );
				date.setMilliseconds( 0 );

				if( date.getTime() <= 0 ) {
					ngModel.$setValidity( "defaultValue", undefined );
				}

				return date;
			} );

			ngModel.$parsers.push( function( value ) {
				var timestamp = new Date( value ).getTime();
				var model = attrs["ngModel"];

				Object.applyStringVal( scope, model, timestamp );

				return timestamp;
			} );
		}
	};
});