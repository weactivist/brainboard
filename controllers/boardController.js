app.controller( "boardController", [ "$scope", "$routeParams", "$timeout", "firebaseFactory", function( $scope, $routeParams, $timeout, firebaseFactory ) {
	$scope.board = undefined;
	$scope.posts = undefined;
	$scope.columnId = undefined;
	$scope.text = "";
	$scope.loading = true;

	firebaseFactory().ref( "/boards/" + $routeParams.id ).on( "value", function( board ) {
		$timeout( function() {
			$scope.board = board.val();
			$scope.posts = getSynchronizedArray( firebaseFactory().ref( "/boards/" + $routeParams.id + "/posts" ) );
			$scope.loading = false;
		} );
	} );

	$scope.add = function( columnId ) {
		$scope.columnId = columnId;
	};
	
	$scope.cancel = function() {
		$scope.columnId = undefined;
		$scope.text = "";
	};

	$scope.like = function( post ) {
		var likes = new Likes();

		if( !likes.Exists( post.$id ) ) {
			post.likes += 1;
			likes.Set( post.$id );
		}
		else {
			post.likes -= 1;
			likes.Remove( post.$id );
		}

		$scope.posts.$set( post.$id, post );
	};

	$scope.isLiked = function( post ) {
		return new Likes().Exists( post.$id );
	};

	$scope.remove = function( post ) {
		$scope.posts.$remove( post.$id );
	};

	$scope.save = function() {
		if( !$scope.text || !$scope.columnId ) {
			$scope.cancel();
			return;
		}

		$scope.posts.$add( {
			text: $scope.text,
			column: $scope.columnId,
			likes: 0
		} );
		
		$scope.cancel();
	};

	$scope.colSize = function() {
		switch( Object.keys( $scope.board.columns ).length ) {
			case 1: return 12;
			case 2: return 6;
			default: return 4;
		}
	};
} ] );