app.controller( "createController", [ "$scope", "$timeout", "$location", "firebaseFactory", function( $scope, $timeout, $location, firebaseFactory ) {
	$scope.name = "";
	$scope.columns = [];
	$scope.errors = [];
	$scope.loading = false;

	$scope.addColumn = function() {
		$scope.columns.push( "" );
	};

	$scope.save = function() {
		$scope.errors = [];

		if( !$scope.name ) {
			$scope.errors.push( "Give your board a name." );
			return;
		}

		if( $scope.columns.length <= 0 ) {
			$scope.errors.push( "Add one or more columns." );
			return;
		}
		else {
			for( var i = 0; i < $scope.columns.length; i++ ) {
				if( !$scope.columns[i] ) {
					$scope.errors.push( "One of your columns is missing a name." );
					return;
				}
			}
		}

		$scope.loading = true;

		var ref = firebaseFactory().ref( "/boards" ).push( {
			name: $scope.name
		}, function( error ) {
			if( error ) {
				$scope.errors.push( error.message );
			}
			else {
				for( var i = 0; i < $scope.columns.length; i++ ) {
					ref.child( "columns" ).push( {
						name: $scope.columns[i],
						posts: {}
					}, function( error ) {
						if( error ) {
							$scope.errors.push( error.message );
						}
					} );
				}

				$timeout( function() {
					$location.path( "/board/" + ref.key );
				} );
			}

			$scope.loading = false;
		});
	};

	$scope.addColumn();
} ] );